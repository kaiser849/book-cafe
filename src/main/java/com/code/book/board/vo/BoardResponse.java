package com.code.book.board.vo;

import java.util.List;

public class BoardResponse {

	private List<BoardVO> data;

	public List<BoardVO> getData() {
		return data;
	}

	public void setData(List<BoardVO> data) {
		this.data = data;
	}
	
}
