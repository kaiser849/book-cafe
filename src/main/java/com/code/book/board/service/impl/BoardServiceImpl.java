package com.code.book.board.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.code.book.board.mapper.BoardMapper;
import com.code.book.board.service.BoardService;
import com.code.book.board.vo.BoardVO;


@Service
public class BoardServiceImpl implements BoardService {

	@Autowired
	BoardMapper mapper;

	@Override
	public List<BoardVO> list() throws Exception {
		return mapper.list();
	}
	
}
