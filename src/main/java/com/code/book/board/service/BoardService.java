package com.code.book.board.service;

import java.util.List;

import com.code.book.board.vo.BoardVO;

public interface BoardService {

	public List<BoardVO> list() throws Exception;
}
