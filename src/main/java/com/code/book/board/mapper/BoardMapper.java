package com.code.book.board.mapper;

import java.util.List;

import com.code.book.board.vo.BoardVO;


public interface BoardMapper {

	public List<BoardVO> list() throws Exception;

}
