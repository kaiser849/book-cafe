package com.code.book.board.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.code.book.board.service.BoardService;
import com.code.book.board.vo.BoardResponse;
import com.code.book.board.vo.BoardVO;

@RestController
public class ApiController {

	@Autowired
	BoardService service;
	
	@GetMapping("/board/list")
	public ResponseEntity<BoardResponse> getBoardList() throws Exception {
		
		List<BoardVO> list =  service.list();
		BoardResponse res = new BoardResponse();
		res.setData(list);
		return new ResponseEntity<>(res, HttpStatus.OK);
	}
	
}
